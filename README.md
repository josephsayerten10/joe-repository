# About My App

For this task, I have decided to work in C#, as I had some experience with it already and it appeared to be a good choice for reading and writing external files.

The app was built in Microsoft Visual Studio with the package manager NuGet, and the app itself makes use of the external package NewtonSoft.Json. This is used to transform the provided class object into a JSON string, which is then written to an external file.

The program itself takes an input of a filepath from the user, which is then used by the app to source the CSV file, read the stream from it and convert it into a string, which is then split by it's lines (or entries).
After filtering out null entries (ie headers and blank rows), the app processes each entry and inputs it into a larger object of 'entries', either by adding it as a child of a previously established parent within the object or by creating new parents and adding the entry as a child of said parent.
Once this overarching object has been filled in with all entries, the app converts the collection of provided entries into a JSON string, modifies the string for the filepath provided initially by the user to replace '.exe' with '_JSONoutput.json', thus creating a new file, and writes the JSON string to this new file.

As for unit tests, I have written unit tests to cover the logic of taking in parameters from the entries and processing them as children within the overarching object.
The cases I have covered are for null parameters, valid single parameters, multiple children, multiple children of a single parent and 4 'parent-child' levels, ie great great grandchildren.

# Linux Instructions

First, you will need to clone a local version of the repository in order to get access to the changes made.

In order to run the app and tests on Linux, you will need to install .NET Core and the package NewtonSoft.

A full guide on how to install .NET Core, tailored for various Linux distributions, can be found here:
https://docs.microsoft.com/en-us/dotnet/core/install/linux

Once this is installed, we can now use dotnet to download the NewtonSoft package, with the command `dotnet add package Newtonsoft.Json --version 12.0.3`.
NOTE: This will need to be run every time the Linux machine is booted up prior to running the application.

Once this has been done, you can navigate to the project folders and run the app and tests from their respective folders.

From the base 'tech-test' folder, to run the app, simply navigate into the folder 'CSVtoJSONconverter' on your Linux machine and run the command `dotnet run`, which will run any applications found within the project folder, in this case our main app.

NOTE: You will be prompted to add the file-path for the CSV you want to convert. Since we are using dotnet however, this will be appended by the current folder path to the CSVtoJSONconverter folder. To access the example data file provided in the folder above, simply input here `../data.csv` and the app should produce a JSON file in the same folder as the data.csv file named data_JSONoutput.json.

In order to run the unit tests, from the 'tech-test' folder, simply go to the directory 'ProgramTest' and run the command `dotnet test`. This should run all unit tests in the given project folder.

# Personal Progress Tracker

(These were just notes I was taking while undergoing the task to show my over-arching process)

To start this task, I have simply created a local folder in which to work and git cloned the repository into this folder.
As my decisions and progress need to be documented, I have also created this covering note as a markdown file, which will eventually replace the provided README file as per the instructions.

The goal is to design an app which can take an input of a CSV file and return a JSON file structured in a parent-child format.
I've decided to create this app in C# using Microsoft Visual Studio.

First, I need to ensure the app can read a given csv file.
For now, I shall simply have it read the provided CSV directly, and then later determine how to generalize it for any given CSV input.
This can simply be done with a StreamReader, pointing to the csv file location.
Having appended ReadToEnd() to the StreamReader and placing the resulting string inside a Console.WriteLine(...), I was able to run the app and confirm it was successfully reading the file.

Our next step is to convert this given Stream into a JSON format, structured as demonstrated.
A quick look at the file in both Excel and pure text format shows it arranges each entry through it's separate levels, from parent to final child.
At each level, is stores data on the name, ID and URL of each item, starting purely with a base URL.
For example, if 'parent' contained 'child1' and 'child2', these would be written as:

```
baseURL, parent_name, 0, parenturl.com, child1_name, 1, child1url.com
baseURL, parent_name, 0, parenturl.com, child2_name, 2, child2url.com
```

First, let's split this stream up by lines, thus separating the entries from each other.
This is done with a simple Split using the new line character '/n' to define a new Array of Strings containing each line from the original.
This is verified by printing the number of elements in the new array and checking it matches the original file, which it does (for extra validitiy, I also checked the first line to make sure it matched what was expected).

Now we have all the lines from the file, but not all of these are entries, some are merely breaks for readability or an index at the start for what each element represents.
For the purposes of this task, this is where the Base URL comes in handy, as all entries listed share the same Base URL, meaning we can filter out any rows whose Base URL doesn't match that of all entries.
Since we still need to separate the entries into their individual components though, it's worth applying this filter to this step to skip unnecessary lines instead of outright deleting them, which would mess up array positions of lines and potentially cause mayhem.
These can again be filtered using Split on the line with character ',' and checking the first element, which is the Base URL.
If it passes, we can send it on to be processed as an entry, for which we will need to create a new class to process.

In the new class for each entry, we need to define three strings for the entry name, ID and URL and also an array of entries for each of the entries children.
We also need to add an initializer for the class to define these parameters for each entry (except for the children, which we shall initially set as a new empty array).
With this, we can now define a base Entry in the program to add our entries to as children.

Using if conditions and foreach loops to check on each level whether a given entry already exists, we can either add new entries to this level or add an entry as a child of an existing parent through each level.
This logic as currently written works, but is admittedly fairly verbose, so later I will return to refine it.
Once we have our collection, we now need to convert it into JSON.

Using NuGet as our package manager for C#, we can download NewtonSoft's JSON Framework, which contains methods allowing us to serialize class objects as JSON text strings, as well as formatting these strings to be indented in order to be readable.
By simply printing the output, we can confirm that it has successfully converted the object to JSON format, and as such the CSV file has now been converted to JSON format.

There are still a few adjustments we need to make though. For one, we need to allow the selection of different CSV files as opposed to pulling directly from the given example.
Then, we need to fix the conversion from CSV to object to accommodate more than 3 levels of parentage, just in case a given CSV file has entries with more than 3 parent-child levels.
We also still need to find a way to properly output the JSON file as opposed to simply printing it on a console line.
On top of this, we need to add unit tests to ensure proper functionality.

First, let's fix the end problem of exporting the string to a JSON file. This can simply be done with a System.IO.File.WriteAllText command for now, however when we generalize the source CSV file so that we can save it in the same location for ease of access, we will need to adjust the file location accordingly.
My solution to allowing different CSV file selections was to add an initial prompt for the user to input the file path into the program, which would then be collected via the StreamReader. This path is stored as a string, and as such we can also manipulate it to find the folder containing the file and opt to save our json in the same location for simplicities sake.

To allow different base URLs while also filtering out headers and blank spaces, I simply replaced my equality check for the given base URL with a StartsWith check to see if it starts with "http", thus allowing different links while filtering out unnecessary data.

To accommodate more than 3 levels of parent-child, I simply extracted the logic I was using before to create child entries for parents into two separate methods, processChild and processChildRecursor, both of which take inputs of the given parameters, reducing the parents at each iteration, and the entry for which the children are to be added (with data found in the parameters). Add some simple null parameter conditions to end the recursion and we can accommodate csv files with more than 3 parent-child levels.

While there may still be improvable areas, we now have an app that appears to fulfill it's purpose. In order to confirm that, though, we first need to add unit tests.

First, I created a new project within the solution to store the unit tests and added the original project as a reference to it. Then, I added into the original files non-static callers of the static processChild method in order for it to be called from the test file.
Since the start of the program simply involves sourcing the CSV file, reading it and splitting it into it's separate rows, this isn't within the scope of logic-testing. When we have our separate entry rows, we want to test the cases of invalid entries (ie blank rows) as well as valid ones (including multiple children, duplicate entries and children of children of children) to make sure they are processed into the correct Entry object. The program ends by converting this object to JSON and publishing it, which again isn't logic based so isn't in-scope for logic-testing. This means the scope of our testing is covered by the processChild and processChildRecursor methods, which we can now access via caller methods.

I have as such written test cases for null parameters, simple valid parameters, multiple children, multiple children under a single parent and a 4-level entry (up to great great grandchildren). This, I feel, covers the core functionality of the method.

With this, we have a complete, functional app, and even though minor adjustments could still potentially be made, I think it's time to publish and move on to the next stage of ensuring it can run on a Linux machine. For this, I will be using Ubuntu.

From the folder this readme is contained in, the .exe file to run the program is located in CSVtoJSONconverter/bin/Release/netcoreapp3.0, following this path takes us to our executable CSVtoJSONconverter.exe, which we can simply run in Ubuntu with `./CSVtoJSONconverter.exe`. It will ask us for the file path of our CSV document including extension (as it is designed to in the app) and from there, the app runs and either creates or updates a JSON file in the same directory as the CSV document and bearing the name of the CSV document appended with "_JSONoutput".

In order to run the tests on Ubuntu, we need to download the .NET Core SDK on Ubuntu, for which I will provide instructions in this readme. When this was installed, I was then able to go into the ProgramTest file and simply run the command `dotnet test`, which would run all the unit tests located in any test project files in the current directory. I was also in fact able to run the actual app itself in a similar way, by going to it's project folder and inputting `dotnet run`, however this caused a problem with inputting the file location as the path file ended up being preceded by the path file to the project folder. To overcome this for the example CSV file, I was simply able to write `../data.csv`, since it was located in the folder above, however obviously this would not come intuitively, especially when instructed otherwise by the app itself.

To accommodate for this, rather than making drastic adjustments to how the code interprets input file paths, I have simply added an extra few lines to be printed on the console upon app launch, stating that if the app is being run through a runtime system such as dotnet (the one I have been using), the user should instead input the file location relative to the current directory, which I then print for the user. It's perhaps not the most elegant solution, but it at least addresses the issue.

Now, I am almost ready to commit the beginner stage of my app. First, I simply need to provide simple documentation on how to run the app and tests on Linux (which I will do in this README), save this file over the README containing my instructions and then I can commit my work. With that, I believe for now, this concludes the Progress section of my notes.