﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace CSVtoJSONconverter
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please copy the full file path of your CSV file into the command line, including file extension");
            Console.WriteLine("NOTE: If running this app through a runtime system such as dotnet, please instead write the file path relative to your current directory:");
            Console.WriteLine(Environment.CurrentDirectory);
            Console.WriteLine("(Use .. to move back up one folder if necessary)");
            String filePath = Console.ReadLine();
            StreamReader sr = new StreamReader(@filePath);

            String csvtext = sr.ReadToEnd();

            String[] csvlines = csvtext.Split('\n');

            Entry entries = new Entry("baseName", "baseID", "baseURL");

            foreach (String s in csvlines)
            {
                String[] parameters = s.Split(',');
                if (parameters.GetValue(0).ToString().StartsWith("http"))
                {
                    String[] newParams = new String[parameters.Length - 1];
                    for (int i = 0; i < parameters.Length - 1; i++)
                    {
                        newParams.SetValue(parameters.GetValue(i + 1), i);
                    }
                    ProcessChild(newParams, entries);
                }
            }
            String json = JsonConvert.SerializeObject(entries.GetChildren()[0], Formatting.Indented);

            String filePathExtensionCropped = filePath.Remove(filePath.Length - 4, 4);

            System.IO.File.WriteAllText(filePathExtensionCropped + "_JSONoutput.json", json);
        }

        static void ProcessChild(String[] parameters, Entry entry)
        {
            if (parameters.Length < 3 || parameters.GetValue(0).Equals(""))
            {
                return;
            }
            Boolean newEntry = true;
            foreach (Entry child in entry.GetChildren())
            {
                if (child.GetName().Equals(parameters.GetValue(0)) &&
                    child.GetID().Equals(parameters.GetValue(1)) &&
                    child.GetURL().Equals(parameters.GetValue(2)))
                {
                    newEntry = false;
                    ProcessChildRecursor(parameters, child);
                }
            }
            if (newEntry)
            {
                Entry entryToAdd = new Entry(parameters.GetValue(0).ToString(),
                                parameters.GetValue(1).ToString(),
                                parameters.GetValue(2).ToString());
                ProcessChildRecursor(parameters, entryToAdd);
                entry.AddChild(entryToAdd);
            }
        }

        static void ProcessChildRecursor(String[] parameters, Entry child)
        {
            String[] newParams = new String[parameters.Length - 3];
            for (int i = 0; i < parameters.Length - 3; i++)
            {
                newParams.SetValue(parameters.GetValue(i + 3), i);
            }
            ProcessChild(newParams, child);
        }

        public void ProcessChildTestAccessor(String[] parameters, Entry entry)
        {
            ProcessChild(parameters, entry);
        }
    }
    public class Entry
    {
        public String label, id, link;
        public Entry[] children;
        public Entry(String name, String ID, String URL)
        {
            this.label = name;
            this.id = ID;
            this.link = URL;
            children = new Entry[] { };
        }

        public String GetName()
        {
            return label;
        }

        public String GetID()
        {
            return id;
        }

        public String GetURL()
        {
            return link;
        }

        public Entry[] GetChildren()
        {
            return children;
        }

        public void AddChild(Entry entry)
        {
            Entry[] childrenNew = new Entry[children.Length + 1];
            for(int i=0; i< children.Length; i++)
            {
                childrenNew.SetValue(children.GetValue(i), i);
            }
            childrenNew.SetValue(entry, children.Length);
            children = childrenNew;
        }
    }
}
