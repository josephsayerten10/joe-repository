using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSVtoJSONconverter;
using System;

namespace CSVtoJSONconverterTest
{
    [TestClass]
    public class ProgramTest
    {
        Program program = new Program();
        
        [TestMethod]
        public void ProcessChild_NullParameters_ReturnSameEntryChildren()
        {
            String[] parameters = new String[] {"","",""};
            Entry entry = new Entry("name", "ID", "URL");

            program.ProcessChildTestAccessor(parameters, entry);

            Assert.AreEqual(0, entry.GetChildren().Length);
        }

        [TestMethod]
        public void ProcessChild_ValidParameters_AddsChild()
        {
            String[] parameters = new String[] { "CHILDNAME", "12345", "https://www.google.co.uk" };
            Entry entry = new Entry("name", "ID", "URL");

            program.ProcessChildTestAccessor(parameters, entry);

            Assert.AreEqual(1, entry.GetChildren().Length);
            Entry result = entry.GetChildren()[0];
            Assert.AreEqual("CHILDNAME", result.GetName());
            Assert.AreEqual("12345", result.GetID());
            Assert.AreEqual("https://www.google.co.uk", result.GetURL());
        }

        [TestMethod]
        public void ProcessChild_MultipleValidParameters_AddsMultipleChildren()
        {
            String[] parameters1 = new String[] { "CHILDNAME1", "1", "https://www.google.co.uk" };
            String[] parameters2 = new String[] { "CHILDNAME2", "2", "https://www.bing.co.uk" };
            Entry entry = new Entry("name", "ID", "URL");

            program.ProcessChildTestAccessor(parameters1, entry);
            program.ProcessChildTestAccessor(parameters2, entry);

            Assert.AreEqual(2, entry.GetChildren().Length);
            Entry result1 = entry.GetChildren()[0];
            Entry result2 = entry.GetChildren()[1];
            Assert.AreEqual("CHILDNAME1", result1.GetName());
            Assert.AreEqual("1", result1.GetID());
            Assert.AreEqual("https://www.google.co.uk", result1.GetURL());
            Assert.AreEqual("CHILDNAME2", result2.GetName());
            Assert.AreEqual("2", result2.GetID());
            Assert.AreEqual("https://www.bing.co.uk", result2.GetURL());
        }

        [TestMethod]
        public void ProcessChild_MoreThanThreeValidParameters_AddsChildrenOfChildren()
        {
            String[] parameters = new String[] { 
                "CHILDNAME", "1", "https://www.google.co.uk", 
                "GRANDCHILDNAME", "2", "https://www.bing.co.uk", 
                "GREATGCNAME", "3", "https://www.yahoo.co.uk", 
                "GGGCNAME", "4", "https://www.ask.co.uk" };
            Entry entry = new Entry("name", "ID", "URL");

            program.ProcessChildTestAccessor(parameters, entry);

            Assert.AreEqual(1, entry.GetChildren().Length);
            Entry result = entry.GetChildren()[0];
            Assert.AreEqual("CHILDNAME", result.GetName());
            Assert.AreEqual("1", result.GetID());
            Assert.AreEqual("https://www.google.co.uk", result.GetURL());
            Assert.AreEqual(1, result.GetChildren().Length);
            Entry gcresult = result.GetChildren()[0];
            Assert.AreEqual("GRANDCHILDNAME", gcresult.GetName());
            Assert.AreEqual("2", gcresult.GetID());
            Assert.AreEqual("https://www.bing.co.uk", gcresult.GetURL());
            Assert.AreEqual(1, gcresult.GetChildren().Length);
            Entry ggcresult = gcresult.GetChildren()[0];
            Assert.AreEqual("GREATGCNAME", ggcresult.GetName());
            Assert.AreEqual("3", ggcresult.GetID());
            Assert.AreEqual("https://www.yahoo.co.uk", ggcresult.GetURL());
            Assert.AreEqual(1, ggcresult.GetChildren().Length);
            Entry gggcresult = ggcresult.GetChildren()[0];
            Assert.AreEqual("GGGCNAME", gggcresult.GetName());
            Assert.AreEqual("4", gggcresult.GetID());
            Assert.AreEqual("https://www.ask.co.uk", gggcresult.GetURL());
        }

        [TestMethod]
        public void ProcessChild_MultipleParametersSameParent_AddsMultipleChildrenForSameParent()
        {
            String[] parameters1 = new String[] { 
                "CHILDNAME", "1", "https://www.google.co.uk", 
                "GC1", "2", "https://www.bing.co.uk" };
            String[] parameters2 = new String[] { 
                "CHILDNAME", "1", "https://www.google.co.uk", 
                "GC2", "3", "https://www.yahoo.co.uk" };
            Entry entry = new Entry("name", "ID", "URL");

            program.ProcessChildTestAccessor(parameters1, entry);
            program.ProcessChildTestAccessor(parameters2, entry);

            Assert.AreEqual(1, entry.GetChildren().Length);
            Entry result = entry.GetChildren()[0];
            Assert.AreEqual("CHILDNAME", result.GetName());
            Assert.AreEqual("1", result.GetID());
            Assert.AreEqual("https://www.google.co.uk", result.GetURL());
            Assert.AreEqual(2, result.GetChildren().Length);
            Entry gcresult1 = result.GetChildren()[0];
            Entry gcresult2 = result.GetChildren()[1];
            Assert.AreEqual("GC1", gcresult1.GetName());
            Assert.AreEqual("2", gcresult1.GetID());
            Assert.AreEqual("https://www.bing.co.uk", gcresult1.GetURL());
            Assert.AreEqual("GC2", gcresult2.GetName());
            Assert.AreEqual("3", gcresult2.GetID());
            Assert.AreEqual("https://www.yahoo.co.uk", gcresult2.GetURL());
        }
    }
}
